<?php

namespace app\models;


use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
   
    public static function tableName(){
		return 'user';
	}
	
	public $role;
   
	public function rules()
	{
		return
		[
			
				
				[['username','password','authKey','CategoryId'],'string','max' => 255],
				[['username','password',],'required'],
				[['username'],'unique'],
				[['role'], 'safe'],
			
			
		];
	}
	
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		return  self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
       throw new NotSupportedException('Not Supported');
       
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
		return  self::findOne(['username'=> $username]);
	}

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
		
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
				generatePasswordHash($this->password);

		// passwd encryption		
        if ($this->isNewRecord)
		    $this->authKey = Yii::$app->security->generateRandomString(32);
		
		// check that category manager is not changing the category
		if (!$this->isNewRecord){
			$auth = Yii::$app->authManager;
			$role = $auth->getRolesByUser($this->id);
			if($role == 'category_manager'){
				if(self::findOne($this->id)->CategoryId != $this->CategoryId)
					 $this->addError($CategoryId, 'You cannot change category you not own');
			}
		}
		

        return $return;
    }	
	
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);
		
		$auth = Yii::$app->authManager;
		
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
				$role = $auth->getRole('not_authorized');
				$auth->assign($role, $this->id);
		}else{
				$role_name = $this->role;
				$role = $auth->getRole($role_name);
				$db = \Yii::$app->db;
				$db->createCommand()->delete('auth_assignment',
					['user_id' => $this->id])->execute();
				$auth->assign($role, $this->id);	
		}	

        return $return;
    }
	
 	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

	public static function getUsers()
	{
		$allUsers = self::find()->all();
		$users = ArrayHelper::
		map($allUsers, 'id', 'username');
		
		return $users;						
	}
	
	public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}
}
