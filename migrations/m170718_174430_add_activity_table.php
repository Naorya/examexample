<?php

use yii\db\Migration;

class m170718_174430_add_activity_table extends Migration
{
    public function up()
    {
		$this->createTable('activity', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
			'categoryId' => $this->integer(),
			'statusId' => $this->integer()
		]);
    }

    public function down()
    {
        $this->dropTable('activity');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
