<?php

use yii\db\Migration;

class m170718_174104_add_CategoryId_to_users extends Migration
{
    public function up()
    {
		$this->addColumn('user','CategoryId','integer');
    }

    public function down()
    {
        $this->dropColumn('user','CategoryId');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
